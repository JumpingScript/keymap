/* Copyright 2020 ZSA Technology Labs, Inc <@zsa>
 * Copyright 2020 Jack Humbert <jack.humb@gmail.com>
 * Copyright 2020 Christopher Courtney, aka Drashna Jael're  (@drashna) <drashna@live.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



#include QMK_KEYBOARD_H
#include "version.h"
#include "keymap_german.h"
#include "sendstring_german.h"
#define MOON_LED_LEVEL LED_LEVEL

enum layers {
    BASE,  // default layer
    SYMB,  // symbols
    MDIA,  // media keys
    BONE, // neo bone
};

enum custom_keycodes {
    VRSN = SAFE_RANGE,
    PHPARR,
    PHPEARR,
};

// enum dance_action {
// };

// clang-format off
const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    [BASE] = LAYOUT_moonlander(
        DE_CIRC     , KC_1,             KC_2,           KC_3,       KC_4,               KC_5, KC_HOME  ,              KC_END      , KC_6,       KC_7,       KC_8,     KC_9,             KC_0   ,          DE_QUES,
        KC_TAB      , KC_Q,             KC_W,           KC_E,       KC_R,               KC_T, KC_DELETE,              KC_BACKSPACE, DE_Z,       KC_U,       KC_I,     KC_O,             KC_P   ,          DE_UDIA,
        KC_CAPS_LOCK, KC_A,             KC_S,           KC_D,       KC_F,               KC_G, PHPARR   ,              PHPEARR     , KC_H,       KC_J,       KC_K,     KC_L,             DE_ODIA,          DE_ADIA,
        KC_LSFT,      DE_Y,             KC_X,           KC_C,       KC_V,               KC_B,                                       KC_N,       KC_M,       KC_COMMA, KC_DOT,           DE_MINS,          KC_RSFT,
        DF(BONE),     KC_UP,            KC_DOWN,        KC_LALT,    KC_LCTL,                   KC_HYPR, KC_ESCAPE,                              KC_RCTL,    DE_SS,    KC_LEFT,          KC_RIGHT,         _______,
                                            KC_SPACE, KC_TAB, KC_LGUI,            MO(MDIA), MO(SYMB), MT(MOD_MEH, KC_ENTER)
    ),
    [SYMB] = LAYOUT_moonlander(
        VRSN,    KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_HOME,           KC_END,  KC_F6,   KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11,
        DE_ASTR, DE_GRV,  DE_AT,   DE_LCBR, DE_RCBR, DE_PIPE, KC_DELETE,         _______, KC_UP,   KC_7,    KC_8,    KC_9,    DE_ASTR, KC_F12,
        DE_TILD, DE_HASH, DE_DLR,  DE_LBRC, DE_RBRC, DE_ACUT, DE_QUOT,           _______, KC_DOWN, KC_4,    KC_5,    KC_6,    DE_PLUS, DE_EURO,
        _______, DE_PERC, DE_CIRC, DE_LABK, DE_RABK, DE_PLUS,                             KC_AMPR, KC_1,    KC_2,    KC_3,    DE_BSLS, _______,
        _______, _______, _______, _______, DE_DLR,          _______,           _______,          _______, KC_DOT,  KC_0,    DE_EQL,  _______,
                                            _______, _______, _______,           _______,_______, _______
    ),
    [MDIA] = LAYOUT_moonlander(
        LED_LEVEL,_______,_______, _______, _______, _______, _______,           _______, _______, _______, _______, _______, _______, QK_BOOT,
        _______, _______, _______, KC_MS_U, _______, _______, _______,           _______, _______, _______, KC_UP,   _______, _______, _______,
        _______, _______, KC_MS_L, KC_MS_D, KC_MS_R, _______, _______,           _______, _______, KC_LEFT, KC_DOWN, KC_RGHT, _______, KC_MPLY,
        _______, _______, _______, _______, _______, _______,                             _______, _______, KC_MPRV, KC_MNXT, _______, _______,
        _______, _______, _______, KC_BTN1, KC_BTN2,         _______,            _______,          KC_VOLU, KC_VOLD, KC_MUTE, _______, _______,
                                            _______, _______, _______,           _______, _______, _______
    ),
    [BONE] = LAYOUT_moonlander(
        KC_MEH      , KC_1,     KC_2,       KC_3,       KC_4,           KC_5,       KC_HOME  ,                      KC_END , KC_6,      KC_7,       KC_8,   KC_9,       KC_0,       DE_QUES,
        KC_HYPR     , KC_J,     KC_D,       KC_U,       KC_A,           KC_X,       KC_DELETE,                      _______, KC_P,      KC_H,       KC_L,   KC_M,       KC_W,       DE_SS  ,
        KC_CAPS_LOCK, KC_C,     KC_T,       KC_I,       KC_E,           KC_O,       PHPARR   ,                      PHPEARR, KC_B,      KC_N,       KC_R,   KC_S,       KC_G,       KC_Q   ,
        KC_LSFT,      _______,  KC_F,       KC_V,       DE_UDIA,        DE_ADIA,                                             DE_ODIA,   DE_Y,       DE_Z,   KC_COMMA,   KC_DOT,     KC_K,
        DF(BASE),     _______,  KC_LEFT,    KC_RGHT,    MT(MOD_LCTL, DE_DLR),                        KC_HYPR, KC_ESCAPE,                _______,    KC_UP,  KC_DOWN,    KC_PSCR,    MO(SYMB),
                                            KC_SPACE, KC_BACKSPACE, KC_LGUI,       KC_LALT, KC_TAB, MT(MOD_MEH, KC_ENTER)
    ),
};

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    if (record->event.pressed) {
        switch (keycode) {
        case VRSN:
            SEND_STRING (QMK_KEYBOARD "/" QMK_KEYMAP " @ " QMK_VERSION);
            return false;
        case PHPARR:
            SEND_STRING ("->");
            return false;
        case PHPEARR:
            SEND_STRING ("=>");
            return false;
        }
    }
    return true;
}

// Tap Dance definitions
// tap_dance_action_t tap_dance_actions[] = {
// };

extern rgb_config_t rgb_matrix_config;

void keyboard_post_init_user(void) {
  rgb_matrix_enable();
}

const uint8_t PROGMEM ledmap[][RGB_MATRIX_LED_COUNT][3] = {
    [0] = { {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255}, {188,255,255} },

    [1] = { {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255}, {41,255,255} },

    [2] = { {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206}, {74,255,206} },

    [3] = { {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255}, {131,255,255} },

};

void set_layer_color(int layer) {
  for (int i = 0; i < RGB_MATRIX_LED_COUNT; i++) {
    HSV hsv = {
      .h = pgm_read_byte(&ledmap[layer][i][0]),
      .s = pgm_read_byte(&ledmap[layer][i][1]),
      .v = pgm_read_byte(&ledmap[layer][i][2]),
    };
    if (!hsv.h && !hsv.s && !hsv.v) {
        rgb_matrix_set_color( i, 0, 0, 0 );
    } else {
        RGB rgb = hsv_to_rgb( hsv );
        float f = (float)rgb_matrix_config.hsv.v / UINT8_MAX;
        rgb_matrix_set_color( i, f * rgb.r, f * rgb.g, f * rgb.b );
    }
  }
}

bool rgb_matrix_indicators_user(void) {
  if (keyboard_config.disable_layer_led) { return false; }
  switch (biton32(layer_state)) {
    case 0:
      set_layer_color(0);
      break;
    case 1:
      set_layer_color(1);
      break;
    case 2:
      set_layer_color(2);
      break;
    case 3:
      set_layer_color(3);
      break;
   default:
    if (rgb_matrix_get_flags() == LED_FLAG_NONE)
      rgb_matrix_set_color_all(0, 0, 0);
    break;
  }
  return true;
}

layer_state_t default_layer_state_set_user(layer_state_t state) {
    switch (get_highest_layer(state)) {
    case BASE:
        set_layer_color(0);
        break;
    case BONE:
        set_layer_color(3);
        break;
    }
  return state;
}
